(defproject repo-manager "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "GNU General Public License v3.0"
            :url "http://www.gnu.org/licenses/gpl-3.0.txt"
            :year 2016
            :key "gpl-3.0"}
  :dependencies [[org.clojure/clojure "1.8.0"]]
  :main ^:skip-aot repo-manager.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
