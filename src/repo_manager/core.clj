(ns repo-manager.core
  (:require [clojure.java.io :as io])
  (:gen-class))


(defn strip-comment
  "Strip comments from a line"
  [line]
  (let [n (.indexOf line "#")]
    (if (neg? n) line (subs line 0 n))))

(defn process-file
  "process line at a time"
  [file-name line-func line-acc]
  (with-open [rdr (io/reader file-name)]
    (reduce line-func line-acc (line-seq rdr))))

(defn process-line
  "Analyze a line builds a map of sections"
  [sections line-in]
  (let [line (strip-comment line-in) ;; get rid of comments
        num-sections (count sections)
        section (re-find #"^\[.*\]" line)
        checkout (second (re-find #"^[ \t]*checkout.*=[ \t]*(.*)$" line))
        chain (second (re-find #"^[ \t]*chain.*=[ \t]*(.*)$" line))]
    (cond
      section (assoc-in sections [(keyword (str "section" (inc num-sections))) :name] section)
      checkout (assoc-in sections [(keyword (str "section" num-sections)) :checkout] checkout)
      chain (assoc-in sections [(keyword (str "section" num-sections)) :chain] chain)
      :else sections)))

;; lets capture the .mrconfigile

(def sections (process-file "../../.mrconfig" process-line {}))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))
